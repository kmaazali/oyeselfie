<?php

namespace App\Api\V1\Controllers;

use App\User;
use PHPMailer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ForgotPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ForgotPasswordController extends Controller
{
    public function sendResetEmail(ForgotPasswordRequest $request)
    {
        $mail = new PHPMailer();
        $mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'juz4womenapp@gmail.com';                 // SMTP username
        $mail->Password =  '4Inova.com';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->setFrom('rizk76779@gmail.com', 'Mailer');
        $mail->addAddress($request['email']);     // Add a recipient
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Forgot Password';
        $mail->Body    = ' <b>in bold!</b>';
        //if(!$mail->send()) {
           // return response()->json(['success' => false, 'message' => 'User Already Exists'], 406);//gives error if user exist

       // } else {
           // return response()->json(['success' => true, 'message' => 'Your request for forgot password submitted successfully! Please check your Email.'], 200);//gives message of forgot email

       // }
        //$broker = $this->getPasswordBroker();
        //$sendingResponse = $broker->sendResetLink($request->only('email'));
        //dd($sendingResponse);
        return Password::broker($request ['email']);
        $user = User::where('email', '=', $request->get('email'))->first();

        if(!$user) {
            throw new NotFoundHttpException();
        }

        $broker = $this->getPasswordBroker();
        $sendingResponse = $broker->sendResetLink($request->only('email'));


        if($sendingResponse !== Password::RESET_LINK_SENT) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok'
        ], 200);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    private function getPasswordBroker()
    {
        return Password::broker();
    }
}
