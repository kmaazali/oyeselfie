<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = User::where('email', $request['email'])->with('role')->first();// checks user already exist or not
        if ($user) {
            return response()->json(['success' => false, 'message' => 'User Already Exists'], 406);//gives error if user exist
        } else {
            $user = new User($request->all());
            $user->save();


            if (!Config::get('boilerplate.sign_up.release_token')) {
                return response()->json([
                    'status' => 'ok',
                    'user' => $user,//takes user
                ], 201);
            }

            $token = $JWTAuth->fromUser($user);
            return response()->json([
                'status' => 'ok',
                'user' => $user,
                'token' => $token
            ], 201);
        }
    }
}
