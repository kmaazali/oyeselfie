<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
    protected $table= 'contest';

    protected $fillable=['title','description','is_active'];
}
