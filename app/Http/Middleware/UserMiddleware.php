<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->role_id != 2)//checks requests data admin role
        {
            return response()->json(['success'=>false, 'message'=>'Your not allowed to access this area'],401);
        }
        return $next($request);
    }
}
