<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username'=>'faizannabi',
            'name'=>'fazy',
            'email'=>'test@test.com',
            'password'=>'123456789',
            'role_id'=>1,
            'is_suspended'=>0

        ]);

    }
}
